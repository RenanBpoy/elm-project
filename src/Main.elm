module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (class, placeholder, src, style, type_, value)
import Html.Events exposing (onClick, onInput)

-- definição dos tipos usados na aplicação

type Rating
    = Excellent
    | Good
    | Average
    | Poor
    | Terrible

type alias Book =
    { title : String
    , author : String
    , publicationYear : String
    , rating : Rating
    }

type alias Model =
    { books : List Book
    , currentBook : String
    , currentAuthor : String
    , currentYear : String
    , currentRating : Rating
    }

type Msg
    = AddBook
    | UpdateBook String
    | UpdateAuthor String
    | UpdateYear String
    | UpdateRating Rating

-- iniciando model com os valores vazios

init : Model
init =
    { books = []
    , currentBook = ""
    , currentAuthor = ""
    , currentYear = ""
    , currentRating = Good
    }

-- função para alterar os valores baseados nas mensagens recebidas

update : Msg -> Model -> Model
update msg model =
    case msg of
        AddBook ->
            let
                newBook =
                    { title = model.currentBook
                    , author = model.currentAuthor
                    , publicationYear = model.currentYear
                    , rating = model.currentRating
                    }
                updatedModel = { model | books = newBook :: model.books, currentBook = "", currentAuthor = "", currentYear = "", currentRating = Good }
            in
            updatedModel
        UpdateBook newBook ->
            { model | currentBook = newBook }
        UpdateAuthor newAuthor ->
            { model | currentAuthor = newAuthor }
        UpdateYear newYear ->
            { model | currentYear = newYear }
        UpdateRating newRating ->
            { model | currentRating = newRating }

-- função para mostrar a interface

view : Model -> Html Msg
view model =
    div []
        [ div [ class "input-container" ]
            [ input [ placeholder "Insira o nome do livro", type_ "text", onInput UpdateBook, value model.currentBook] []
            ]
        , div [ class "input-container" ]
            [ input [ placeholder "Insira o autor", type_ "text", onInput UpdateAuthor, value model.currentAuthor] []
            ]
        , div [ class "input-container" ]
            [ input [ placeholder "Insira o ano de publicação", type_ "text", onInput UpdateYear, value model.currentYear] []
            ]
        , div [ class "input-container" ]
            [ select [ onInput (\value -> UpdateRating (stringToRating value)), value (model.currentRating |> ratingToString) ]
                [ option [ value (Excellent |> ratingToString) ] [ text "Excelente" ]
                , option [ value (Good |> ratingToString) ] [ text "Bom" ]
                , option [ value (Average |> ratingToString) ] [ text "Médio" ]
                , option [ value (Poor |> ratingToString) ] [ text "Ruim" ]
                , option [ value (Terrible |> ratingToString) ] [ text "Péssimo" ]
                ]
            ]
        , div []
            [ button [ class "btn", onClick AddBook ] [ text "Adicionar" ]
            ]
        , div []
            (List.map (\book -> li [] [ bookCard book ]) model.books)
        ]




-- modelo do card que recebe um livro para criar o card com as informações

bookCard : Book -> Html Msg
bookCard book =
    div [ class "card" ]
        [ div [ class "card-body" ]
            [ div [ class "row" ]
                [ div [ class "col-md-4" ]
                    [ img
                        [ src "img/livro.png"
                        , style "max-width" "150px"
                        , style "max-height" "150px"
                        ]
                        []
                    ]
                , div [ class "col-md-8" ]
                    [ h5 [ class "card-title text-left" ] [ text ("Título: " ++ book.title) ]
                    , p [ class "card-text text-left" ] [ text ("Autor: " ++ book.author) ]
                    , p [ class "card-text text-left" ] [ text ("Ano de Publicação: " ++ book.publicationYear) ]
                    , p [ class "card-text text-left" ] [ text ("Avaliação: " ++ (book.rating |> ratingToString)) ]
                    ]
                ]
            ]
        ]

-- conversão do tipo da dropdown para string

ratingToString : Rating -> String
ratingToString rating =
    case rating of
        Excellent -> "Excelente"
        Good -> "Bom"
        Average -> "Médio"
        Poor -> "Ruim"
        Terrible -> "Péssimo"

-- conversão do tipo da string para elemento da dropdown

stringToRating : String -> Rating
stringToRating value =
    case value of
        "Excelente" -> Excellent
        "Bom" -> Good
        "Médio" -> Average
        "Ruim" -> Poor
        "Péssimo" -> Terrible
        _ -> Good

main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }
