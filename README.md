Nome: Renan Bordignon Poy

- Projeto

O programa foi criado a partir da linguagem Elm, uma linguagem utilizada para construção de front-ends que compila para JavaScript.
O projeto apresenta 4 arquivos principais. O index.html é a interface que será mostrada ao usuário, recebendo componentes do 
arquivo Main.elm (compilado para o arquivo elm.js) que é responsável pela lógica das funções e mandar mensagens para essa interface.
Em adicional temos o arquivo styles.css que é responsável pelos estilos da página, e o elm.json que é o arquivo responsável por indicar
que é uma aplicação Elm.

- Código

O código em sua maior parte é feito no arquivo Main.elm, em que ele define os tipos de dados usados na aplicação nos com "type", e então entra
nas funções. A função init é responsável por inicializar os dados vazios, enquanto a função update recebe dados do tipo msg e atualiza os dados
da aplicação baseado na mensagem. Em seguida, o programa possui 2 funções semelhantes, que apenas alteram o tipo do dado recebido da dropdown de avaliação
de rating para string ou de string para rating. A função bookCard é apenas uma função que recebe o dado do livro e estrutura ele no formato de um card
da biblioteca boostrap. Por fim, a função "principal" view, que é responsável por gerar a estrutura que será colocada na interface html do programa.
Além do Main.elm, no index.html tem uma pequena seção script para que o index se comunique com a estrutura criada pelo Elm

- Execução

Após compilar o arquivo Main.elm com o comando elm make Main.elm --output=elm.js (arquivo já compilado no envio, então essa etapa pode ser pulada), é necessário um
servidor local para rodar a aplicação. Neste caso foi escolhido um servidor python, que pode ser executado com o comando python -m http.server. Com o servidor
rodando, já é possível utilizar a aplicação no endereço gerado (normalmente localhost:8000).

- Escolhas

Para a construção do html, escolhi utilizar a biblioteca do bootstrap, que fornece uma estrutura de grid responsivo.
Para realizar funções de alteração nos dados, foi escolhido um sistema que alterava a partir de mensagens dependendo do evento realizado

- Dificuldades

Uma ideia que teve que ser deixada de lado foi a de carregar a imagem do livro junto aos outros dados, mas devido a este ter sido o primeiro
contato com a linguagem elm, tive dificuldades e não consegui implementar corretamente o carregamento de imagem, deixando apenas uma imagem de um livro
em cada card

- Link para o screencast

https://www.loom.com/share/a2ec5b2e7a3d491d91e468e5bebce9ff

- Referências

https://elmprogramming.com
https://elmprogramming.com/building-a-simple-page-in-elm.html
https://getbootstrap.com/docs/5.3/getting-started/introduction/
